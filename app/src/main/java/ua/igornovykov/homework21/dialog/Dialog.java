package ua.igornovykov.homework21.dialog;

public class Dialog {

    private String userProfileImage;
    private String name;
    private String descriptionMessage;
    private String time;

    public Dialog(String userProfileImage ,String name, String descriptionMessage, String time) {
        this.userProfileImage = userProfileImage;
        this.name = name;
        this.descriptionMessage = descriptionMessage;
        this.time = time;
    }

    public String getUserProfileImage() {
        return userProfileImage;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescriptionMessage() {
        return descriptionMessage;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

}
