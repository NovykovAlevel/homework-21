package ua.igornovykov.homework21.dialog;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.bumptech.glide.request.RequestOptions;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import ua.igornovykov.homework21.GlideApp;
import ua.igornovykov.homework21.R;

public class DialogsAdapter extends RecyclerView.Adapter<DialogsAdapter.DialogViewHolder> {


    protected List<Dialog> dialogs;
    protected DialogsAdapterClickListener listener;

    public DialogsAdapter(List<Dialog> dialogs) {
        this.dialogs = dialogs;
    }

    public void addDialog(Dialog dialog) {
        dialogs.add(dialog);
        notifyDataSetChanged();
    }

    public void setListener(DialogsAdapterClickListener listener) {
        this.listener = listener;
    }

    @NonNull
    @Override
    public DialogViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(viewGroup.getContext());
        View itemView = layoutInflater.inflate(R.layout.item_dialog, viewGroup, false);
        return new DialogViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull DialogViewHolder dialogViewHolder, int position) {
        Dialog dialog = dialogs.get(position);
        dialogViewHolder.bind(dialog);
    }

    @Override
    public int getItemCount() {
        return dialogs.size();
    }

    class DialogViewHolder extends RecyclerView.ViewHolder {
        protected CircleImageView contactImageView;
        protected TextView nameTitleTextView;
        protected TextView dialogDescriptionTextView;
        protected TextView timeTextView;


        public DialogViewHolder(@NonNull View itemView) {
            super(itemView);
            nameTitleTextView = itemView.findViewById(R.id.nameTitleTextView);
            contactImageView = itemView.findViewById(R.id.contactImageView);
            dialogDescriptionTextView = itemView.findViewById(R.id.dialogDescriptionTextView);
            timeTextView = itemView.findViewById(R.id.timeTextView);
        }

        public void bind(final Dialog dialog) {
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onClicked(dialog);
                }
            });
            nameTitleTextView.setText(dialog.getName());
            loadUserImage(dialog);
            dialogDescriptionTextView.setText(dialog.getDescriptionMessage());
            timeTextView.setText(dialog.getTime());

        }

        private void loadUserImage(Dialog dialog) {
            GlideApp
                    .with(itemView.getContext())
                    .load(dialog.getUserProfileImage())
                    .apply(RequestOptions.circleCropTransform())
                    .into(contactImageView);
        }
    }
}
