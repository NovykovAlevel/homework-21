package ua.igornovykov.homework21.dialog;

public interface DialogsAdapterClickListener {

    void onClicked(Dialog dialog);
}
