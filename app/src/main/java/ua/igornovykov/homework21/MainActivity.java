
package ua.igornovykov.homework21;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import ua.igornovykov.homework21.dialog.Dialog;
import ua.igornovykov.homework21.dialog.DialogsAdapter;
import ua.igornovykov.homework21.dialog.DialogsAdapterClickListener;
import ua.igornovykov.homework21.message.MessageActivity;

public class MainActivity extends AppCompatActivity {

    protected RecyclerView dialogRecyclerView;
    protected Button addDialogButton;
    protected Toolbar toolbar;
    public static String userIcon1 = "https://i.kym-cdn.com/photos/images/original/000/442/063/e7c.png";
    public static String userIcon2 = "https://www.freepngimg.com/thumb/my_little_pony/9-2-my-little-pony-free-png-image-thumb.png";
    public static String userIcon3 = "https://www.clipartmax.com/png/middle/74-749348_siren-pony-by-l-kazumi-l-mlp-adagio-dazzle-pony.png";
    public static String userDefaultIcon = "https://mbtskoudsalg.com/images/like-emoji-png-8.png";

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        setTitle("Messenger");


        dialogRecyclerView = findViewById(R.id.dialogRecyclerView);
        addDialogButton = findViewById(R.id.addDialogButton);

        List<Dialog> dialogs = new ArrayList<>(Arrays.asList(
                new Dialog(userIcon1, "Sam Smith", "Hello Jack!", "21:58"),
                new Dialog(userIcon2, "James Kotlin", "Where are you?", "13:12"),
                new Dialog(userIcon3, "Helga Falcon", "Android best of the best", "10:20")
        ));
        final DialogsAdapter adapter = new DialogsAdapter(dialogs);
        adapter.setListener(new DialogsAdapterClickListener() {
            @Override
            public void onClicked(Dialog dialog) {
                Intent intent = new Intent(MainActivity.this, MessageActivity.class);
                intent.putExtra("icon", dialog.getUserProfileImage());
                intent.putExtra("name", dialog.getName());
                intent.putExtra("message", dialog.getDescriptionMessage());
                startActivity(intent);
            }
        });

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(
                this,
                LinearLayoutManager.VERTICAL,
                false);
        dialogRecyclerView.setLayoutManager(layoutManager);
        dialogRecyclerView.setAdapter(adapter);

        addDialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                adapter.addDialog(new Dialog(userDefaultIcon, "Name Surname", "new message", "time"));
            }
        });

    }

}
