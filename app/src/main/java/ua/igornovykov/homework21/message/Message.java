package ua.igornovykov.homework21.message;

public class Message {
    private String friendProfileIcon;
    private String descriptionMessage;
    private MessageType messageType;

    public Message(String descriptionMessage) {
        this(descriptionMessage,null, MessageType.My);
    }

    public Message(String descriptionMessage,String friendProfileIcon , MessageType messageType) {
        this.friendProfileIcon = friendProfileIcon;
        this.descriptionMessage = descriptionMessage;
        this.messageType = messageType;
    }

    public String getFriendProfileIcon() {
        return friendProfileIcon;
    }

    public String getDescriptionMessage() {
        return descriptionMessage;
    }

    public MessageType getMessageType() {
        return messageType;
    }
}

