package ua.igornovykov.homework21.message;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import ua.igornovykov.homework21.R;

public class MessageAdapter extends RecyclerView.Adapter<MessageAdapter.MessageViewHolder> {

    protected List<Message> messages;


    public MessageAdapter(List<Message> messages) {
        this.messages = messages;
    }

    public void addMyMessage(Message message) {
        messages.add(message);
        notifyItemInserted(messages.size() - 1);
    }

    @NonNull
    @Override
    public MessageViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(viewGroup.getContext());
        MessageType messageType = MessageType.values()[viewType];
        if (messageType == MessageType.Friend) {
            View itemView = layoutInflater.inflate(R.layout.item_friend_message, viewGroup, false);
            return new MessageViewHolder(itemView);
        }
        if (messageType == MessageType.My) {
            View itemView = layoutInflater.inflate(R.layout.item_my_message, viewGroup, false);
            return new MessageViewHolder(itemView);
        }
        throw new IllegalStateException("Unknown value of MessageType");
    }

    @Override
    public void onBindViewHolder(@NonNull MessageViewHolder messageViewHolder, int position) {
        Message message = messages.get(position);
        messageViewHolder.bind(message);
    }

    @Override
    public int getItemCount() {
        return messages.size();
    }

    @Override
    public int getItemViewType(int position) {
        Message message = messages.get(position);
        return message.getMessageType().ordinal();
    }

    class MessageViewHolder extends RecyclerView.ViewHolder {
        protected CircleImageView contactImageView;
        protected TextView messageDescriptionTextView;
        protected TextView timeTextView;

        public MessageViewHolder(@NonNull View itemView) {
            super(itemView);
            contactImageView = itemView.findViewById(R.id.contactImageView);
            messageDescriptionTextView = itemView.findViewById(R.id.messageDescriptionTextView);
            timeTextView = itemView.findViewById(R.id.timeTextView);
        }

        public void bind(final Message message) {
            if (message.getMessageType() == MessageType.Friend) {
                loadUserImage(message);
            }
            messageDescriptionTextView.setText(message.getDescriptionMessage());
        }

        private void loadUserImage(Message message) {
            Glide.with(itemView.getContext()).load(message.getFriendProfileIcon()).into(contactImageView);
        }
    }
}
