package ua.igornovykov.homework21.message;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import ua.igornovykov.homework21.R;


public class MessageActivity extends AppCompatActivity {

    protected RecyclerView messageRecyclerView;
    protected Toolbar upToolbar;
    protected EditText messageEditText;
    protected ImageButton smileImageButton;
    protected ImageButton clipImageButton;
    protected ImageButton enterImageButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);
        messageRecyclerView = findViewById(R.id.messageRecyclerView);
        smileImageButton = findViewById(R.id.smileImageButton);
        clipImageButton = findViewById(R.id.clipImageButton);
        enterImageButton = findViewById(R.id.enterImageButton);
        messageEditText = findViewById(R.id.messageEditText);
        upToolbar = findViewById(R.id.upToolbar);

        setSupportActionBar(upToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        checkOnSendMessage();

        Intent intent = getIntent();
        String friendImage = intent.getStringExtra("icon");
        String name = intent.getStringExtra("name");
        final String message = intent.getStringExtra("message");
        setTitle(name);

        List<Message> messages = new ArrayList<>(Arrays.asList(
                new Message("Hello Jack!",friendImage, MessageType.Friend),
                new Message("Where are you?", friendImage,MessageType.Friend),
                new Message("Android best of the best", friendImage,MessageType.Friend)
        ));
        final MessageAdapter messageAdapter = new MessageAdapter(messages);
        enterImageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String myMessageContent = messageEditText.getText().toString();
                messageAdapter.addMyMessage(new Message(myMessageContent));
                messageEditText.setText("");
                messageRecyclerView.scrollToPosition(messageAdapter.getItemCount() - 1);
            }
        });

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(
                this,
                LinearLayoutManager.VERTICAL,
                false);
        messageRecyclerView.setLayoutManager(layoutManager);
        messageRecyclerView.setAdapter(messageAdapter);
    }

    private void checkOnSendMessage() {
        messageEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String text = s.toString();
                if (!text.trim().isEmpty()) {
                    enterImageButton.setEnabled(true);
                } else {
                    enterImageButton.setEnabled(false);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }
}
